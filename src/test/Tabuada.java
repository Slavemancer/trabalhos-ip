package test;

import javax.swing.JOptionPane;

public class Tabuada {

	public static void main(String[] args) {
		int numeroInicial = Integer.parseInt(JOptionPane.showInputDialog("Numero",2));
		//int numeroTabuadas = Integer.parseInt(JOptionPane.showInputDialog("Tabuadas",3));
		final int numeroTabuadas = 3;
		int numerosTabuadas[] = new int[numeroTabuadas];
		int tamanhoTabuadas[] = new int[numeroTabuadas];
		String ts[] = new String[numeroTabuadas];
		String outputFinal;
		for (int i = 0; i < numeroTabuadas; i++) {
			numerosTabuadas[i] = numeroInicial+2*i;			
			tamanhoTabuadas[i] = (numerosTabuadas[i]+ " * " +10+ " = " +(numerosTabuadas[i]*10)).length();
			ts[i] = "%-"+(tamanhoTabuadas[i]+2)+"s ";
		}
		numeroInicial=2;
		for (int i = 1; i <= 10; i++) {
			for (int j = 0; j < numeroTabuadas; j++) {
				outputFinal=i + " * " + numerosTabuadas[j] + " = "+ i*numerosTabuadas[j];
				System.out.printf(ts[j],outputFinal);
			}
			System.out.println();			
		}
	}

}
